<?php

namespace Dcms\Orders\Database\Seeds;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class OrdersPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = ['orders'];
        $permission_actions = ['browse', 'read', 'edit', 'add', 'delete'];

        foreach ($modules as $module) {
            Permission::firstOrCreate(['name' => $module, 'module' => $module, 'action' => '', 'level' => 0]);

            foreach ($permission_actions as $permission_action) {
                Permission::firstOrCreate(['name' => $module . '-' . $permission_action, 'module' => $module, 'action' => $permission_action, 'level' => 1]);
            }
        }
    }
}
