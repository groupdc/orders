<?php

namespace Dcms\Orders\Http\Controllers;

use DB;
use Auth;
use View;
use Input;
use Session;
use Mandrill;
use Redirect;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dcms\Orders\Models\Dealer;
use Dcms\Orders\Models\Checkout;
use App\Http\Controllers\Controller;
use Dcms\Orders\Models\CarrierLabel;
use Dcms\Orders\Models\Ordersupport;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Http\Controllers\Shop\ProcessController;
use App\Http\Controllers\Shop\Process\SAPController;
use App\Http\Controllers\Shop\Process\EmailController;
use App\Http\Controllers\Shop\ProcessOrderCarrierController;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-browse')->only('index');
        $this->middleware('permission:orders-edit')->only(['edit', 'detail']);
    }

    public function index()
    {
        //echo "de naturapy index";
        return View::make('dcms::orders/index');
    }

    public function edit($invoice_id)
    {
        $Checkout = Checkout::with('vouchers')->with('products')->with('support')->where("invoice_id", "=", $invoice_id)->get();
        if (strlen(trim($Checkout[0]->dealer_code_NL)) > 0) {
            $Dealer = Dealer::where('code_NL', '=', $Checkout[0]->dealer_code_NL)->first();
        } else {
            $Dealer = Dealer::where('code', '=', $Checkout[0]->dealer_code)->first();
        }
        
        $supportstatus = Ordersupport::distinct()->pluck('status');
        
        $Dealers = Dealer::where(function ($q) {
            $q->where('naturapy', '=', 1)->orWhere('naturapypickup', '=', 1);
        })->where('country_id', '=', $Checkout[0]->country_id)
        ->orWhere('code' , '101395')
        ->orWhereRaw('code in (SELECT dealer_code collate utf8_unicode_ci FROM api_authentication WHERE test = 0)')
        ->orderBy('dealer')
        ->orderBy('city')
        ->get();

        return View::make('dcms::orders/edit')
                        ->with('Checkout', $Checkout)
                        ->with('Dealer', $Dealer)
                        ->with('Dealers', $Dealers)
                        ->with('supportstatus', $supportstatus);
    }

    public function update(Request $request, $invoice_id)
    {
        $deliverydate = date_create_from_format('d/m/Y H:i:s', $request->get('deliverydate'). '00:00:00');
        
        $original = Checkout::where("invoice_id", "=", $invoice_id)->first();
        $log = "";
        if ($original->email <> $request->get('email')) {
            $log .= "\r\n email: ". $original->email ." -> ". $request->get('email') ;
        }
        if ($original->firstname <> $request->get('firstname')) {
            $log .= "\r\n firstname: ". $original->firstname ." -> ". $request->get('firstname') ;
        }
        if ($original->lastname <> $request->get('lastname')) {
            $log .= "\r\n lastname: ". $original->lastname ." -> ". $request->get('lastname') ;
        }
        if ($original->street <> $request->get('street')) {
            $log .= "\r\n street: ". $original->street ." -> ". $request->get('street') ;
        }
        if ($original->nr <> $request->get('nr')) {
            $log .= "\r\n nr: ". $original->nr ." -> ". $request->get('nr') ;
        }
        if ($original->box <> $request->get('box')) {
            $log .= "\r\n box: ". $original->box ." -> ". $request->get('box') ;
        }
        if ($original->zip <> $request->get('zip')) {
            $log .= "\r\n zip: ". $original->zip ." -> ". $request->get('zip') ;
        }
        if ($original->city <> $request->get('city')) {
            $log .= "\r\n city: ". $original->city ." -> ". $request->get('city') ;
        }

        if ($original->shipment_firstname <> $request->get('shipment_firstname')) {
            $log .= "\r\n shipment_firstname: ". $original->shipment_firstname ." -> ". $request->get('shipment_firstname') ;
        }
        if ($original->shipment_lastname <> $request->get('shipment_lastname')) {
            $log .= "\r\n shipment_lastname: ". $original->shipment_lastname ." -> ". $request->get('shipment_lastname') ;
        }
        if ($original->shipment_street <> $request->get('shipment_street')) {
            $log .= "\r\n shipment_street: ". $original->shipment_street ." -> ". $request->get('shipment_street') ;
        }
        if ($original->shipment_nr <> $request->get('shipment_nr')) {
            $log .= "\r\n shipment_nr: ". $original->shipment_nr ." -> ". $request->get('shipment_nr') ;
        }
        if ($original->shipment_box <> $request->get('shipment_box')) {
            $log .= "\r\n shipment_box: ". $original->shipment_box ." -> ". $request->get('shipment_box') ;
        }
        if ($original->shipment_zip <> $request->get('shipment_zip')) {
            $log .= "\r\n shipment_zip: ". $original->shipment_zip ." -> ". $request->get('shipment_zip') ;
        }
        if ($original->shipment_city <> $request->get('shipment_city')) {
            $log .= "\r\n shipment_city: ". $original->shipment_city ." -> ". $request->get('shipment_city') ;
        }

        if ($original->language <> $request->get('language')) {
            $log .= "\r\n language: ". $original->language ." -> ". $request->get('language') ;
        }
        if ($original->phone <> $request->get('phone')) {
            $log .= "\r\n phone: ". $original->phone ." -> ". $request->get('phone') ;
        }
        if ($original->delivery_date <> date_format($deliverydate, 'Y-m-d')) {
            $log .= "\r\n deliverydate: ". $original->delivery_date ." -> ". date_format($deliverydate, 'Y-m-d') ;
        }
        if ($original->dealer_code <> $request->get('dealer')) {
            $log .= "\r\n dealer: ". $original->dealer_code ." -> ". $request->get('dealer') ;
        }
        if ($original->delivery <> $request->get('delivery')) {
            $log .= "\r\n delivery: ". $original->delivery ." -> ". $request->get('delivery') ;
        }
        $cancelled = $request->has('cancelled')?1:0;
        if ($original->cancelled <> $cancelled) {
            $log .= "\r\n cancelled: ". $original->cancelled ." -> ". $cancelled ;
        }
        
        if ($request->has('CreateBackOrder')) {
            $log .= "\r\n CREATED BACKORDER  ".$invoice_id.'_back' ;
        }

        Checkout::where("invoice_id", "=", $invoice_id)
            ->update([
                'email' => $request->get('email'),
                'firstname' => $request->get('firstname'),
                'lastname' => $request->get('lastname'),
                'street' => $request->get('street'),
                'nr' => $request->get('nr'),
                'box' => $request->get('box'),
                'zip' => $request->get('zip'),
                'city' => $request->get('city'),
                'language' => $request->get('language'),
                'phone' => $request->get('phone'),
                'delivery_date' => date_format($deliverydate, 'Y-m-d'),
                'delivery_unixdate' => date_format($deliverydate, 'U'),
                'delivery' => $request->get('delivery'),
                
                'shipment_firstname' => $request->get('shipment_firstname'),
                'shipment_lastname' => $request->get('shipment_lastname'),
                'shipment_street' => $request->get('shipment_street'),
                'shipment_nr' => $request->get('shipment_nr'),
                'shipment_box' => $request->get('shipment_box'),
                'shipment_zip' => $request->get('shipment_zip'),
                'shipment_city' => $request->get('shipment_city'),
                'dealer_code' => $request->get('dealer'),

                'cancelled' => $cancelled,
            ]);

        $request->request->add(['comment' => '['.Carbon::now().' SYSTEM:'.Auth::guard('dcms')->user()->username.']'. "\r\n" . ' Update customer details '.$log. "\r\n\r\n" . $request->get('comment')]);
        $this->changeStatus($request, $invoice_id, false);

        if ($request->has('CreateBackOrder')) {
            $Checkouts = Checkout::where("invoice_id", "=", $invoice_id)->get();

            $newdelivery = $Checkouts->first()->delivery;
            $newinvoice_idprefix = $invoice_id;

            /*if ($request->has('changeDelivery') && $request->get('changeDelivery') == 1) {
                if ($original->delivery == 1) {
                    $newdelivery = 0;
                    $newinvoice_idprefix = str_replace('D', 'P', $invoice_id);
                } elseif ($original->delivery == 0) {
                    $newdelivery = 1;
                    $newinvoice_idprefix = str_replace('P', 'D', $invoice_id);
                }
            }*/

            $checkForInvoiceId = Checkout::where('invoice_id', 'LIKE', $newinvoice_idprefix.'%')->get();
            $unique = $checkForInvoiceId->unique('invoice_id');
            $ExistingInvoices = array();
            foreach ($unique->values()->all() as $x) {
                $ExistingInvoices[] = $x->invoice_id;
            };
            if (!in_array($newinvoice_idprefix.'_back', $ExistingInvoices)) {
                $newinvoice_id = $newinvoice_idprefix.'_back';
            } else {
                for ($x=1; $x<=99; $x++) {
                    $newinvoice_id = $newinvoice_idprefix.'_back'.$x;
                    if (!in_array($newinvoice_id, $ExistingInvoices)) {
                        break;
                    }
                }
            }

            Checkout::where("invoice_id", "=", $invoice_id)->update(['backorder'=>$newinvoice_id.' '.$original->backorder]);
            
            $V = array();
            if ($request->has('product.V')) {
                $V = $request->get('product')['V'];
            }
            $atLeastOneReplica = false;
            foreach ($Checkouts as $C) {
                $replicate = false;
                
                $quantity = 0;
                if (isset($C->voucher) && count($V)>0 && in_array($C->product_code, $V)) {
                    $replicate = true;
                    // every loop we need to eliminate the $reques var since with vouchers this can lead to errors
                    $keyindex = array_search($C->product_code, $V);
                    unset($V[$keyindex]);
                    $quantity = intval($request->get('productquantity')['V'][$keyindex]);
                } elseif (!isset($C->voucher) && $request->has('product.P') && in_array($C->product_code, $request->get('product')['P'])) {
                    $replicate = true;
                    $keyindex = array_search($C->product_code, $request->get('product')['P']);
                    $quantity = intval($request->get('productquantity')['P'][$keyindex]);
                }

                if ($quantity > $C->quantity) {
                    $quantity = $C->quantity;
                }

                if ($replicate == true && $quantity>0) {
                    $newC = $C->replicate();
                    $newC->quantity = $quantity;
                    $newC->invoice_id = $newinvoice_id;
                    $newC->price = 0;
                    // $newC->delivery_cost = 0; // To Do
                    $newC->delivery = $request->get('delivery');
                    $newC->delivery_date = date_format(date_create_from_format('d/m/Y H:i:s', $request->get('deliverydate'). '00:00:00'), 'Y-m-d');
                    $newC->delivery_unixdate = date_format(date_create_from_format('d/m/Y H:i:s', $request->get('deliverydate'). '00:00:00'), 'U');
                    $newC->dealer_code = $request->get('dealer');
                    $newC->backorder_original = $invoice_id;
                    $newC->payed = 1;
                    $newC->dealer_code = $request->get('dealer');

                    if (!empty($newC->shipment_street)) {
                        $newC->shipment_address = 1;
                    }
                    $r = $newC->save();
                    $atLeastOneReplica = true;
                }
            }
            //make the DPD/PostNL label (cron handles this)
            //make the SAP order() ==> specific call to SAPController
            
            if ($atLeastOneReplica == true) {
                try {
                    SAPController::createOrder($newinvoice_id);
                } catch (Exception $ex) {
                    Bugsnag::notifyException($ex);
                }

                // A DPD label will be created by cron
                // based on the payed indicator, and the fact there is no related label
                                
                try {
                    EmailController::sendOrderConfirmation($newinvoice_id);
                } catch (Exception $ex) {
                    Bugsnag::notifyException($ex);
                }
            }
        }
        
        if ($request->has('SaveWithLabel')) {
            try {
                $poc = new ProcessOrderCarrierController();
                $poc->makeLabelForInvoicde($invoice_id);
            } catch (Exception $ex) {
                Bugsnag::notifyException($ex);
            }
            return redirect(route('admin/orders/edit', [$invoice_id]))->with('message', 'Update successful!');
        }

        if ($request->has('CreateBackOrder')) {
            return redirect(route('admin/orders/edit', [$invoice_id]))->with('message', 'Backorder created - '.$newinvoice_id);
        }
        
        return redirect(route('admin/orders/edit', [$invoice_id]))->with('message', 'Update successful!');
    }

    public function detail($invoice_id = null)
    {
        $Checkout = Checkout::with('vouchers')->with('products')->with('support')->where("invoice_id", "=", $invoice_id)->get();
        if (strlen(trim($Checkout[0]->dealer_code_NL)) > 0) {
            $Dealer = Dealer::where('code_NL', '=', $Checkout[0]->dealer_code_NL)->first();
        } else {
            $Dealer = Dealer::where('code', '=', $Checkout[0]->dealer_code)->first();
        }
        $supportstatus = Ordersupport::distinct()->pluck('status');
        $carrierlabel = CarrierLabel::where('invoice_id', $Checkout->first()->invoice_id)->get();
        
        return View::make('dcms::orders/detail')
                        ->with('Checkout', $Checkout)
                        ->with('Dealer', $Dealer)
                        ->with('supportstatus', $supportstatus)
                        ->with('carrierlabel', $carrierlabel);
    }

    public function changeStatus(Request $request, $invoice_id = null, $return = true)
    {
        $msg = '<p class="alert">Support comment is saved';
        if ($request->has('sentEmail') && $request->get('sentEmail') == 1) {
            $PCtrl = new EmailController();
            $PCtrl->sendOrderConfirmation($invoice_id, true);
            $msg .= '<br/>The e-mail has been sent';

            $support = Ordersupport::firstOrNew(['invoice_id'=>$invoice_id]);
            $support->status = $request->get('status');
            $support->comment = $support->comment ."\r\n\r\n". "resent email confirmation" ."\r\n\r\n";
            $support->save();
        }
        $msg .= "</p>";
        
        $Orders = Checkout::where("invoice_id", "=", $invoice_id)->get();
        if (strtolower($Orders->first()->language) != strtolower($request->get('language'))) {
            foreach ($Orders as $Order) {
                $Order->language = strtolower($request->get('language'));
                $Order->save();
            }
        }
        $support = Ordersupport::firstOrNew(['invoice_id'=>$invoice_id]);
        $support->status = $request->get('status');
        $support->comment = $support->comment ."\r\n\r\n". $request->get('comment');
        $support->save();
        if ($return == true) {
            Session::flash('message', $msg);
            return Redirect::back();
        }
    }

    public function mandrillView($invoice_id = null)
    {
        $Checkout = Checkout::with('vouchers')->where("invoice_id", "=", $invoice_id)->first();
        $date_range = 30;
        $mandrill = new Mandrill();
        $query = 'sender:"dcm@dcm-info.com" AND subject:"' . $invoice_id . '" ';//Input::get("query"); //'subject:"DCM in de media? Ja zeker!" AND u_list_name:"DCM Retail BENL" AND u_list_id:36';
        $date_to = date("Y-m-d", time());// '2015-02-01';
        $date_from = date("Y-m-d", (time() - ($date_range * 86400)));//'2015-03-01';
        $Mandrillresult = $mandrill->messages->search($query, $date_from, $date_to, null, null);//($query, $date_from, $date_to, $tags, $senders);
        return View::make('dcms::orders/mandrillview')->with('Mandrillresult', $Mandrillresult)->with('Checkout', $Checkout);
    }

    public function sales()
    {
        $Checkout = DB::connection('project')->select("SELECT
                                                    (SELECT count(distinct invoice_id) FROM `order` WHERE (delivery_cost > 0 and delivery_cost is not null) AND payed = 1 ) as delivery_orders,
                                                    (SELECT count(distinct invoice_id) FROM `order` WHERE (delivery_cost > 0 and delivery_cost is not null) AND payed = 1 and country_id = 1 ) as delivery_orders_be,
                                                    (SELECT count(distinct invoice_id) FROM `order` WHERE (delivery_cost > 0 and delivery_cost is not null) AND payed = 1 and country_id = 2 ) as delivery_orders_nl,
                                                    (SELECT count(distinct invoice_id) FROM `order` WHERE (delivery_cost = 0 or delivery_cost is null) and payed = 1) as pickup_orders,
                                                    (SELECT count(distinct invoice_id) FROM `order` WHERE (delivery_cost = 0 or delivery_cost is null) and payed = 1 and country_id = 1) as pickup_orders_be,
                                                    (SELECT count(distinct invoice_id) FROM `order` WHERE (delivery_cost = 0 or delivery_cost is null) and payed = 1 and country_id = 2) as pickup_orders_nl,
                                                    (SELECT count(*) from `order` where voucher is not null and payed = 1) as vouchers,
                                                    (SELECT count(*) from `order` where voucher is null and payed = 1) as products,
                                                    (SELECT count(distinct invoice_id) from `order` where voucher is not null and payed = 1) as orders_using_voucher,
                                                    (SELECT count(distinct invoice_id) from `order` where voucher is null and payed = 1) as orders_without_voucher,
                                                    (SELECT count(distinct invoice_id) from `order` where payed = 1) as orders

                                                    ");

        $HighVolumeDays = DB::connection('project')->select("SELECT count(*) as cnt, orderday FROM (
                                                        SELECT date_format(created_at,'%W') as orderday
                                                        FROM `order` where payed = 1
                                                        GROUP BY invoice_id
                                                      ) as T GROUP BY orderday
                                                      ORDER BY 1 desc");

        $TopProducts = DB::connection('project')->select("SELECT count(*) as cnt, product_txt FROM
													(
														SELECT `order`.id, `order`.invoice_id, voucher.product_txt COLLATE utf8_unicode_ci  as product_txt
														FROM `order` INNER JOIN naturapy.voucher ON `order`.voucher = voucher.code
														WHERE order.voucher is not null and payed = 1

														union

														SELECT `order`.id, `order`.invoice_id, vwproductsbenl.product COLLATE utf8_unicode_ci  as product_txt
														FROM `order` INNER JOIN vwproductsbenl ON `order`.product_code COLLATE utf8_unicode_ci = vwproductsbenl.dcmCode COLLATE utf8_unicode_ci
														WHERE order.voucher is null and payed = 1

													) as T
												  GROUP BY product_txt
												  ORDER BY 1 desc");

        $TopProductsViaVoucher = DB::connection('project')->select("SELECT count(*) as cnt, product_txt FROM
                (
                    SELECT `order`.id, `order`.invoice_id, voucher.product_txt
                    FROM `order` INNER JOIN naturapy.voucher ON `order`.voucher = voucher.code
                    WHERE order.voucher is not null and order.payed = 1
                ) as T
              GROUP BY product_txt
              ORDER BY 1 desc");

        $TopProductsNoVoucher = DB::connection('project')->select("SELECT count(*) as cnt, product_txt FROM
				(
                    SELECT `order`.id, `order`.invoice_id, vwproductsbenl.product as product_txt
                    FROM `order` INNER JOIN vwproductsbenl ON `order`.product_code COLLATE utf8_unicode_ci = vwproductsbenl.dcmCode COLLATE utf8_unicode_ci
                    WHERE order.voucher is null and order.payed = 1
                ) as T
              GROUP BY product_txt
              ORDER BY 1 desc");

        $TopDealersBE = DB::connection('project')->select("  SELECT count(*) as cnt,(select count(*) from `order` where dealer_code = T.dealer_code and payed = 1) as cntproducts, code,  dealer, address, zip, city FROM (
                                                                SELECT `order`.id, `order`.invoice_id, `order`.dealer_code , dealers.id as dealerid, dealers.code,dealers.dealer, dealers.address, dealers.zip, dealers.city
                                                                FROM `order`
                                                                INNER JOIN dcm.dealers on dealers.code = `order`.dealer_code and dealers.country_id = 1
                                                                WHERE (`order`.dealer_code_NL is null or trim(`order`.dealer_code_NL) = '' ) and (`order`.dealer_code is not null and trim(`order`.dealer_code) <> '') AND order.payed = 1
                                                                GROUP by invoice_id
                                                              ) as T group by  dealerid order by 1 desc");

        $TopDealersNL = DB::connection('project')->select("  SELECT count(*) as cnt,(select count(*) from `order` where dealer_code_NL = T.dealer_code_NL and payed = 1) as cntproducts, code_NL, dealer, address, zip, city from (
                                                            SELECT `order`.id, `order`.invoice_id, `order`.dealer_code_NL , dealers.id as dealerid, dealers.dealer,  dealers.code_NL, dealers.address, dealers.zip, dealers.city
                                                            FROM `order`
                                                            INNER JOIN dcm.dealers on dealers.code_NL = `order`.dealer_code_NL  and dealers.country_id = 2
                                                            WHERE `order`.dealer_code_NL is NOT null and trim(`order`.dealer_code_NL) <> '' AND order.payed = 1
                                                            GROUP by invoice_id
                                                          ) as T group by dealerid order by 1 desc");

        $TopDealersNL = DB::connection('project')->select("  SELECT count(*) as cnt,(select count(*) from `order` where dealer_code = T.dealer_code and payed = 1) as cntproducts, code_NL, dealer, address, zip, city from (
                                                        SELECT `order`.id, `order`.invoice_id, `order`.dealer_code , dealers.id as dealerid, dealers.dealer,  dealers.code_NL, dealers.address, dealers.zip, dealers.city
                                                        FROM `order`
                                                        INNER JOIN dcm.dealers on dealers.code = `order`.dealer_code  and dealers.country_id = 2 and payed = 1
                                                        GROUP by invoice_id
                                                      ) as T group by dealerid order by 1 desc");


        return View::make('dcms::orders/sales')
            ->with('Checkout', $Checkout)
            ->with('HighVolumeDays', $HighVolumeDays)
            ->with('TopProducts', $TopProducts)
            ->with('TopProductsViaVoucher', $TopProductsViaVoucher)
            ->with('TopProductsNoVoucher', $TopProductsNoVoucher)
            ->with('TopDealersBE', $TopDealersBE)
            ->with('TopDealersNL', $TopDealersNL);
    }

    public function getDataTable()
    {
        $query = DB::connection('project')
                            ->table("order")
                            ->select(
                                DB::raw(
                                    'group_concat(voucher) as vouchers, 
                                         case `order`.country_id when 2 then \'nl\' when 3 then \'lu\' else \'be\' end as countryDB, 
                                         `order`.invoice_id,
                                         `order`.firstname,
                                         `order`.lastname,
                                         `order`.dealer_code,
                                         `order`.email,
                                         `order`.voucher,
                                         `order`.country,
                                         `order`.zip,
                                         `order`.payed,
                                         concat(ucase(`order`.origin), " ", reference) as origin,
                                         
                                         `dealers`.zip as dealerzip'
                                )
                            )
                            ->leftJoin('dealers', 'order.dealer_code', '=', 'dealers.code')
                                
                            ->groupBy('invoice_id')
                            ->orderBy('order.created_at', 'desc');

        if (intval(session('overrule_default_by_country_id')) > 0) {
            $query->where('order.country_id', intval(session('overrule_default_by_country_id')));
        }
        
        return DataTables::queryBuilder($query)
                            // ->addColumn('tracking', function ($model) {
                            //     if ($model->carrier_id == 1) {
                            //         return '<a href="https://tracking.dpd.de/parcelstatus?query='.$model->reference.'&locale=nl_BE" target="_blank">
                            //     <img src="https://tracking.dpd.de/status/favicon.ico">'.$model->reference.'</a>';
                            //     } elseif ($model->carrier_id == 3 || $model->carrier_id == 4) {
                            //         if (strtolower(substr($model->invoice_id, 0, 1)) == 'p') {
                            //             return '<a href="http://postnl.nl/tracktrace/?T=C&D='.$model->country.'&B='.$model->reference.'&P='.$model->dealerzip.'" target="_blank">
                            //         <img src="https://jouw.postnl.be/track-and-trace/favicon.ico" width="16">'.$model->reference.'</a>';
                            //         } else {
                            //             return '<a href="http://postnl.nl/tracktrace/?T=C&D='.$model->country.'&B='.$model->reference.'&P='.$model->zip.'" target="_blank">
                            //         <img src="https://jouw.postnl.be/track-and-trace/favicon.ico" width="16">'.$model->reference.'</a>';
                            //         }
                            //     }
                            // })
                            ->addColumn('name', function ($model) {
                                return $model->firstname . ' ' . $model->lastname;
                            })
                            ->addColumn('edit', function ($model) {
                                if ($model->countryDB == 'nl') {
                                    return '<a class="btn btn-xs btn-default" href="/admin/orders/detail/' . $model->invoice_id . '"><i class="far fa-eye"></i></a>
                                            <a class="btn btn-xs btn-default" href="/admin/orders/' . $model->invoice_id . '/edit"><i class="far fa-pencil"></i></a>
                                            <a class="btn btn-xs btn-default" title="make new label" target="_blank" href="http://dcm-info.nl/bestellen/carrierlabel/' . $model->invoice_id . '"><i class="far fa-redo"></i></a>';
                                } else {
                                    return '<a class="btn btn-xs btn-default" href="/admin/orders/detail/' . $model->invoice_id . '"><i class="far fa-eye"></i></a>
                                            <a class="btn btn-xs btn-default" href="/admin/orders/' . $model->invoice_id . '/edit"><i class="far fa-pencil"></i></a>
                                            <a class="btn btn-xs btn-default" title="make new label" target="_blank" href="http://dcm-info.be/nl/bestellen/carrierlabel/' . $model->invoice_id . '"><i class="far fa-redo"></i></a>';
                                }
                            })
                            ->rawColumns(['vouchers','tracking','edit'])
                            ->filterColumn('name', function ($query, $keyword) {
                                $query->whereRaw('firstname like ? OR lastname like ?', ["%{$keyword}%","%{$keyword}%"]);
                            })
                            ->make(true);
    }
}
