<?php

namespace Dcms\Orders\Http\Controllers;

use DB;
use Auth;
use View;
use Input;
use Session;
use Mandrill;
use Redirect;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dcms\Orders\Models\Voucher;
use App\Http\Controllers\Controller;

class VouchersController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-browse')->only('index');
        $this->middleware('permission:orders-edit')->only(['edit', 'detail']);
    }

    public function index()
    {
        //echo "de naturapy index";
        return View::make('dcms::vouchers/codes');
    }

    public function destroy($id)
    {
        $v = Voucher::where('id',$id)->first();
        $v->used = 1;
        $v->active = 0;
        $v->save();
        
        Session::flash('message', 'Successfully deleted the dealer!');
        return Redirect::to('admin/vouchers/codes');
    }

    public function getDataTable()
    {
        $query = DB::connection('naturapy')
                            ->table("voucher")
                            ->select(DB::raw('id, code, product_txt, used'));

        return DataTables::queryBuilder($query)
                            ->addColumn('edit', function ($model) {

                                
        $edit = '<form method="POST" action="/admin/vouchers/codes/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">';
        if (Auth::user()->can('dealers-edit')) {
            // $edit .= '<a class="btn btn-xs btn-default" href="/admin/dealers/' . $model->id . '/edit"><i class="far fa-pencil-alt"></i></a>';
        }
        if (Auth::user()->can('dealers-delete')) {
            $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Deactivate this voucher" title="Deactivate this voucher" onclick="if(!confirm(\'Are you sure to deactivate this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
        }
        $edit .= '</form>';

                                return $edit;
                            
                            })
                            ->rawColumns(['edit'])
                            ->make(true);
    }
}
