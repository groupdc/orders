<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin"), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {
            //DASHBOARD - CMS HOME
        
            Route::any("orders/api/table/orders", array('as'=>'admin/orders/api/table/orders', 'uses'=>'OrdersController@getDataTable'));

            Route::any("orders/mandrillview/{ID}", array('as'=>'admin/orders/mandrillview', 'uses'=>'OrdersController@mandrillView'));

            Route::any("orders/detail/{ID?}", array('as'=>'admin/orders/detail', 'uses'=>'OrdersController@detail'));
            Route::post("orders/detail/{ID}/changeStatus", array('as'=>'admin/orders/detail/changeStatus','uses'=>'OrdersController@changeStatus'));
            
            Route::get("orders/{ID}/edit", array('as'=>'admin/orders/edit', 'uses'=>'OrdersController@edit'));
            Route::post("orders/{ID}", array('as'=>'admin/orders/update','uses'=>'OrdersController@update'));

            Route::any("orders/sales", array('as'=>'orders/sales', 'uses'=>'OrdersController@sales'));

            Route::any("vouchers/api/table/vouchers", array('as'=>'admin/vouchers/api/table/vouchers', 'uses'=>'VouchersController@getDataTable'));
            Route::get("vouchers/codes", array('as'=>'vouchers/codes', 'uses'=>'VouchersController@index'));
            Route::delete("vouchers/codes/{id}", array('uses'=>'VouchersController@destroy'));

            Route::any("orders", array('as'=>'orders/naturapy', 'uses'=>'OrdersController@index'));

        });
    });
});
