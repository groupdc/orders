<?php

namespace Dcms\Orders\Models;

use Dcms\Core\Models\EloquentDefaults;

class CarrierLabel extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "carrier_labels";

    public function getCarrierNameAttribute()
    {
        switch ($this->carrier_id) {
            case 1:
                return 'DPD';
                break;
            case 2:
                return 'Mainfreight';
                break;
            case 3:
            case 4:
                return 'PostNL';
                break;
        }
    }
}
