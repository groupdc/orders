<?php

namespace Dcms\Orders\Models;

use Dcms\Core\Models\EloquentDefaults;

class Checkout extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "order";

    public function vouchers()
    {
        return $this->hasOne('Dcms\Orders\Models\Voucher', 'code', 'voucher');
    }

    public function support()
    {
        return $this->hasOne('Dcms\Orders\Models\Ordersupport', 'invoice_id', 'invoice_id');
    }

    public function products()
    {
        return $this->hasMany('Dcms\Orders\Models\Product', 'dcmCode', 'product_code');
    }

     // Relations
     public function carrierLabel()
     {
         return $this->hasMany(CarrierLabel::class, 'invoice_id', 'invoice_id');
     }
     
}
