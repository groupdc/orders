<?php

namespace Dcms\Orders\Models;

use Dcms\Core\Models\EloquentDefaults;

class Dealer extends EloquentDefaults
{
    protected $connection = "project";
    protected $table  = "dealers";
}
