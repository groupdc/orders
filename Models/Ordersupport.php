<?php

namespace Dcms\Orders\Models;

use Dcms\Core\Models\EloquentDefaults;

class Ordersupport extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "order_support";

    protected $fillable = ['invoice_id'];
}
