<?php

namespace Dcms\Orders\Models;

use Dcms\Core\Models\EloquentDefaults;

class Product extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "vwproducts";
}
