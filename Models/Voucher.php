<?php

namespace Dcms\Orders\Models;

use Dcms\Core\Models\EloquentDefaults;

class Voucher extends EloquentDefaults
{
    protected $connection = 'naturapy';
    protected $table  = "voucher";

    public function checkout()
    {
        return $this->hasOne('Checkout', 'voucher', 'code');
    }
}
