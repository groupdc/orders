<?php

return [
    "Orders" => [
        "icon"  => "fa-shopping-basket",
        "links" => [
            ["route" => "admin/orders", "label" => "Orders", "permission" => "orders"],
            ["route" => "admin/voucher-codes", "label" => "Prepaid Codes", "permission" => "orders"],
        ],
    ],
];
