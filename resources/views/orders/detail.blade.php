@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1> Orders </h1>

        <ol class="breadcrumb">
            <li class=""><a href="{{route('orders/naturapy')}}"><i class="far fa-shopping-basket"></i> Orders</a></li>
            <li class="active">Detail ({{$Checkout[0]->invoice_id}})</li>
        </ol>
    </div>

    <div class="main-content" id="mncntnt">
        <div class="row">
            @if(Session::has('message'))
                <div class="col-md-12">
                    {!!Session::get('message')!!}
                </div>
            @endif
            <div class="col-md-12">
                <div class="main-content-block">

                    <form name="frmorderstatus" method="post" action="{{route('admin/orders/detail/changeStatus',$Checkout[0]->invoice_id)}}">
                    {{csrf_field()}}
                        
                    <h3>Customer details</h3>
                    Reference: {{$Checkout[0]->invoice_id}}
                    <br/>
                    Email: {{$Checkout[0]->email}}
                    <br/>
                    Name: {{$Checkout[0]->firstname}}  {{$Checkout[0]->lastname}}
                    <br/>
                    Address: {{$Checkout[0]->street}} {{$Checkout[0]->nr}} {{$Checkout[0]->bus}}
                    <br/>
                    Place: {{$Checkout[0]->zip}} {{$Checkout[0]->city}}
                    <br/>
                    Language: <select id="orderlanguage" name="language">
                                <option value="nl" @if(strtolower($Checkout[0]->language) == "nl")selected @endif > nl </option>
                                <option value="fr" @if(strtolower($Checkout[0]->language) == "fr")selected @endif > fr </option>
                            </select>
                    <br/>
                    Country: {{ ( ($Checkout[0]->country_id == 1) ? 'BE'  : (($Checkout[0]->country_id == 2) ? 'NL' : (($Checkout[0]->country_id == 9) ? 'LU' : '') ) ) }}

                    <br />
                    Phone: {{$Checkout[0]->phone}}
                    <br />
                    Delivery date: <b>{{$Checkout[0]->delivery_date}}</b> @if($Checkout[0]->delivery == 1 ) home
                    delivery @else pick up date @endif

                    <br />
                    Origin: {{strtoupper($Checkout[0]->origin)}} {{$Checkout[0]->reference}}  @if(strtoupper($Checkout[0]->origin) == 'API') (below you'll find the service point) @endif
                    <br />
                    Cancelled: {{$Checkout[0]->cancelled}}

                    <h3>Order details</h3>
                    @foreach($Checkout as $C)
                        Product:  {{$C->quantity}} x  <b>{{$C->products->first()->product}}</b> ({{$C->products->first()->usage}} {{$C->products->first()->usage_intent}})
                        @if(isset($C->voucher))
                            <br/>
                            Voucher: <b>{{$C->voucher}}</b>
                            <br/>
                        @else
                            <br/>
                            Voucher: -
                            <br/>
                        @endif
                        <br/>
                    @endforeach

                    <h3>Tracking details</h3>
                    
                    @if($carrierlabel->count()>0)

                        @foreach($carrierlabel as $label)
                            @if(in_array($label->carrier_id, [3,4] ))
                                <span>Package Tracking: 

                                @if (strtolower(substr($Checkout->first()->invoice_id, 0, 1)) == 'p') 
                                    <a href="http://postnl.nl/tracktrace/?T=C&D={{$Checkout->first()->country}}&B={{$label->reference}}&P={{$Dealer->zip}}" target="_blank">
                                    <img src="https://jouw.postnl.be/track-and-trace/favicon.ico" width="16">{{$label->reference}}</a>
                                @else
                                    <a href="http://postnl.nl/tracktrace/?T=C&D={{$Checkout->first()->country}}&B={{$label->reference}}&P={{$Checkout->first()->zip}}" target="_blank">
                                    <img src="https://jouw.postnl.be/track-and-trace/favicon.ico" width="16">{{$label->reference}}</a>
                                @endif
                                </span><br/>  
                            @elseif(in_array($label->carrier_id, [1] ))
                                <span>Package Tracking: <a href="https://www.dpdgroup.com/be/mydpd/my-parcels/search?lang=nl&parcelNumber={{$label->reference}}" target="_blank">
                                <img src="https://tracking.dpd.de/status/favicon.ico">{{$label->reference}}</a></span><br/>
                            @endif
                        @endforeach
                    @endif
                    
                    @if($Checkout[0]->delivery == 1 )
                        <h3>Service Point (delivery)</h3>
                    @else
                        <h3>Service Point (pickup)</h3>
                    @endif

                    {{$Dealer->dealer}}<br/>
                    {{$Dealer->address}}<br/>
                    {{$Dealer->zip}} {{$Dealer->city}}

                        <h3>E-mail history (including customer, client, customerservice, logistics)</h3>
                        <div id="spinmandrill"><i class="far fa-spinner fa-spin" style="font-size:24px"></i></div>
                        <div id="resultmandrill"></div>

<?php
                        if (($Checkout[0]->country_id == 2)) {
                            if (env('APP_ENV') == 'test') {
                                $url = "http://".env('DOMAIN_NL')."/bestellen/email/".$Checkout[0]->invoice_id;
                            } else {
                                $url = "https://dcm-info.nl/bestellen/email/".$Checkout[0]->invoice_id;
                            }
                        } else {
                            if ($Checkout[0]->language == 'fr') {
                                $routeorder = "commander";
                            } else {
                                $routeorder = "bestellen";
                            }
                            
                            if (env('APP_ENV') == 'test') {
                                $url = "http://".env('DOMAIN_BE')."/".$Checkout[0]->language."/".$routeorder."/email/".$Checkout[0]->invoice_id;
                            } else {
                                $url = "https://dcm-info.be/".$Checkout[0]->language."/".$routeorder."/email/".$Checkout[0]->invoice_id;
                            }
                        }

                        ?>

                        <div id="buttonmail"><button name="sentEmail" value="1">Resent email confirmation ({{$Checkout[0]->language}}) </button></div>

                        <h3>Customer Support Notes</h3>
                        <div class="form-group">
                            <label> Status </label><br/>
                            <input list="orderstatus" name="status" value="@if(!is_null($Checkout[0]->support)){{$Checkout[0]->support->status}}@endif">
                            <datalist id="orderstatus">
                                @foreach($supportstatus as $supportstatusvalue)
                                    <option value="{{$supportstatusvalue}}">
                                @endforeach
                            </datalist> 
                        </div>

                        <div  class="form-group">
                            <label> Comment </label><br/>
                            @if(!is_null($Checkout[0]->support))
                            {!!nl2br($Checkout[0]->support->comment)!!}<br/>
                            @endif
                            <textarea name="comment" cols="100" rows="20">[{{Carbon\Carbon::now()}} {{Auth::guard('dcms')->user()->username}}]
</textarea>
                        </div>
                        <input class="btn btn-primary" type="submit" value="save">
                        <a href="/admin/orders" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content" id="waitformailtobesent">
    <i class="far fa-spinner fa-spin" style="font-size:24px"></i>The system prepares the email and will sent it to all parties, you will get notified about the result. Please wait.
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#waitformailtobesent").hide();

            $.get("/admin/orders/mandrillview/{{$Checkout[0]->invoice_id}}", function (data) {
                $("#spinmandrill").hide();
                $("#resultmandrill").html(data);
            });

            $('#sentnewmail').click(function () {
               alert('A new window will open to send this e-mail, you may close this one window when the text is visible');
            })
        });
    </script>

@stop
