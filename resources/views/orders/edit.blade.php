@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1> Orders </h1>

        <ol class="breadcrumb">
            <li class=""><a href="{{route('orders/naturapy')}}"><i class="far fa-shopping-basket"></i> Orders</a></li>
            <li class="active"> Detail ({{$Checkout[0]->invoice_id}})</li>
        </ol>
    </div>

    <div class="main-content" id="mncntnt">
        <div class="row">
            @if(Session::has('message'))
                <div class="col-md-12">
                {!!Session::get('message')!!}
                </div>
            @endif
            
            <div class="col-md-12">
                <div class="main-content-block">
                    <form name="frmorderstatus" method="post" action="{{route('admin/orders/update',$Checkout[0]->invoice_id)}}">
                    {{csrf_field()}}

                    <div class="form-group">
                    <h3>Customer details</h3> 
                    Reference: <b>{{$Checkout[0]->invoice_id}}</b>
                    </div>
           
                    Products:<br>
                    <table>
                        <tr>
                            <th style="width:100px;">Quantity</th>
                            <th>Name</th>
                            <th>Voucher</th>
                            <th style="text-align:center;">Backorder</th>
                        </tr>
                        
                    
                    @foreach($Checkout as $i => $C)
                        @php
                            if(isset($C->voucher)) {
                                $prefix = 'V';
                            } else {
                                $prefix = 'P';
                            }
                        @endphp
                        <tr>
                            <td style="border-top: 1px solid rgba(0, 0, 0, 0.05); padding:2px 0;"><input type="text" class="form-control"  name="productquantity[{{$prefix}}][{{$i}}]" size ="2" value="{{$C->quantity}}" style="width: auto;"></td>
                            <td style="border-top: 1px solid rgba(0, 0, 0, 0.05)">{{$C->products->first()->product}} ({{$C->products->first()->usage}} {{$C->products->first()->usage_intent}}) </td>
                            <td style="border-top: 1px solid rgba(0, 0, 0, 0.05)">@if(isset($C->voucher)){{$C->voucher}}@else-@endif</td>
                            <td style="border-top: 1px solid rgba(0, 0, 0, 0.05); text-align:center;"><input type="checkbox" class="adminbackorder" name="product[{{$prefix}}][{{$i}}]" value="{{$C->products->first()->dcmCode}}" ></td>
                        </tr>
                    @endforeach
                        </table>
                    <hr/>
                    
                    <div class="form-group">
                    <label>Email:</label><input type="text" class="form-control" name="email" value="{{$Checkout[0]->email}}" />
                    </div>

                    <div class="form-group">
                    <label>Language:</label><select id="orderlanguage" class="form-control" name="language">
                                <option value="nl" @if(strtolower($Checkout[0]->language) == "nl")selected @endif > nl </option>
                                <option value="fr" @if(strtolower($Checkout[0]->language) == "fr")selected @endif > fr </option>
                            </select>
                            </div>
                    

                    <div class="form-group">
                    <label>Phone:</label><input type="text" class="form-control" name="phone" value="{{$Checkout[0]->phone}}" /> 
                    </div>
                    <div class="form-group">
                        <label>Order type:</label><br>
                        <input type="radio" name="delivery" class="changerange" value="0" @if($Checkout[0]->delivery != 1) checked @endif /> <b>Pick-up</b> at Gardencenter
                        <input type="radio" name="delivery" class="changerange" value="1" style="margin-left:15px;" @if($Checkout[0]->delivery == 1) checked @endif> <b>Delivery</b> at home
                    </div>
                    <div class="form-group">
                    <label>Delivery data:</label>
                    <br/><input type="text" name="deliverydate" class="form-control" id="datepicker" value="{{ date_format(date_create_from_format('Y-m-d', $Checkout[0]->delivery_date), 'd/m/Y') }}" size="30">
                    </div>
                    <div class="form-group">
                    <label>Service Point:</label>
                    <br/>
                    <select name="dealer" id="dealer"  class="form-control">
                    @foreach($Dealers as $dealer)
                        <option value="{{$dealer->code}}" @if($dealer->code == $Dealer->code) selected @endif)>{!!$dealer->dealer!!}({!!$dealer->city!!})</option>
                    @endforeach
                    </select>
                    </div>
                    <br/>
                    <div class="row">
                    <div class="col-md-6">
                        <h4>Billing address</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label>Firstname:</label><input type="text" class="form-control" name="firstname" value="{{$Checkout[0]->firstname}}" /></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label>Lastname:</label><input type="text" class="form-control" name="lastname" value="{{$Checkout[0]->lastname}}" /></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                            <div class="form-group"><label>Street:</label><input type="text" class="form-control" name="street" value="{{$Checkout[0]->street}}" /></div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group"><label>Nr:</label><input type="text" class="form-control" name="nr" value="{{$Checkout[0]->nr}}" /></div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group"><label>Box:</label><input type="text" class="form-control" name="box" value="{{$Checkout[0]->box}}" /></div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"><label>Zip:</label><input type="text" class="form-control" name="zip" value="{{$Checkout[0]->zip}}" /></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group"><label>City:</label><input type="text" class="form-control" name="city" value="{{$Checkout[0]->city}}" /></div>
                            </div>
                        </div>
                        <div class="form-group">
                            Country: {{ ( ($Checkout[0]->country_id == 1) ? 'BE'  : (($Checkout[0]->country_id == 2) ? 'NL' : (($Checkout[0]->country_id == 9) ? 'LU' : '') ) ) }}
                        </div>
                        
                    </div>
                    
                    <div class="col-md-6">
                        <h4>Shipping address</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label>Firstname:</label><input type="text" class="form-control" name="shipment_firstname" value="{{$Checkout[0]->shipment_firstname}}" /></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label>Lastname:</label><input type="text" class="form-control" name="shipment_lastname" value="{{$Checkout[0]->shipment_lastname}}" /></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                            <div class="form-group"><label>Street: </label><input type="text" class="form-control" name="shipment_street" value="{{$Checkout[0]->shipment_street}}" /></div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group"><label>Nr:</label><input type="text" class="form-control" name="shipment_nr" value="{{$Checkout[0]->shipment_nr}}" /></div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group"><label>Box:</label><input type="text" class="form-control" name="shipment_box" value="{{$Checkout[0]->shipment_box}}" /></div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"><label>Zip:</label><input type="text" class="form-control" name="shipment_zip" value="{{$Checkout[0]->shipment_zip}}" /></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group"><label>City:</label><input type="text" class="form-control" name="shipment_city" value="{{$Checkout[0]->shipment_city}}" /></div>
                            </div>
                        </div>
                        <div class="form-group">
                            Country: {{ ( ($Checkout[0]->country_id == 1) ? 'BE'  : (($Checkout[0]->country_id == 2) ? 'NL' : (($Checkout[0]->country_id == 9) ? 'LU' : '') ) ) }}
                        </div>
                    </div>
                    </div>
                    <hr/>

                    <h3>Customer Support Notes</h3>
                        <div class="form-group">
                            <label>Order cancelled : </label>
                            <input type="checkbox" name="cancelled" value="1"  @if($Checkout[0]->cancelled == 1) checked @endif> 
                        </div>

                        <div class="form-group">
                            <label>Status:</label>
                            <input class="form-control" list="orderstatus" name="status" value="@if(!is_null($Checkout[0]->support)){{$Checkout[0]->support->status}}@endif">
                            <datalist id="orderstatus">
                                @foreach($supportstatus as $supportstatusvalue)
                                    <option value="{{$supportstatusvalue}}">
                                @endforeach
                            </datalist> 
                        </div>

                        <div  class="form-group">
                            <label>Comment:</label>
                            <textarea name="comment" class="form-control" cols="100" rows="5">[{{Carbon\Carbon::now()}} {{Auth::guard('dcms')->user()->username}}]
                            </textarea>
                            @if(!is_null($Checkout[0]->support))
                            {!!nl2br($Checkout[0]->support->comment)!!}<br/>
                            @endif
                            
                        </div>

                    <br/>
                    <br/>
                        <input class="btn btn-primary" type="submit" name="SaveOnly" value="Save">
                        <input class="btn btn-primary" type="submit" name="SaveWithLabel" value="Save with new label">
                        
                         @if($Checkout[0]->payed == 1)
                        <input class="btn btn-info" type="submit" onClick="return confirm('Are you sure, this will make a new order!\r\nThe user will be notified about this handling, \r\nSAP order will be created, \r\nDPD label will be made');return false;" name="CreateBackOrder" value="Create Backorder">
                        @endif
                        <a href="/admin/orders" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {
            var TodayPlusTwo = new Date();
            // add a day
            TodayPlusTwo.setDate(TodayPlusTwo.getDate() + 2);

            @if($Checkout[0]->delivery != 1)
            $('#datepicker').daterangepicker({
                        singleDatePicker: true,
                        minDate:TodayPlusTwo,
                        locale: {
                            "format": 'DD/MM/YYYY',
                            "firstDay": 1
                            },
                        isInvalidDate: function(date) {
                            if (date._d.getDay() == 0 || date._d.getDay() == 1 || date._d.getDay() == 2 ) return true;
                            return false;
                        }
                    });
            @else
            $('#datepicker').daterangepicker({
                    singleDatePicker: true,
                    minDate:TodayPlusTwo,
                    locale: {
                        "format": 'DD/MM/YYYY',
                        "firstDay": 1
                        },
                    isInvalidDate: function(date) {
                        if (date._d.getDay() == 0 || date._d.getDay() == 1 || date._d.getDay() == 6 ) return true;
                        return false;
                    }
                });
            @endif

            $('.changerange').click(function(){
                

            if($(this).val() == 0) {
            
                $('#datepicker').daterangepicker({
                        singleDatePicker: true,
                        minDate:TodayPlusTwo,
                        locale: {
                            "format": 'DD/MM/YYYY',
                            "firstDay": 1
                            },
                        isInvalidDate: function(date) {
                            if (date._d.getDay() == 0 || date._d.getDay() == 1 || date._d.getDay() == 2 ) return true;
                            return false;
                        }
                    });
            } else {
                    $('#datepicker').daterangepicker({
                    singleDatePicker: true,
                    minDate:TodayPlusTwo,
                    locale: {
                        "format": 'DD/MM/YYYY',
                        "firstDay": 1
                        },
                    isInvalidDate: function(date) {
                        if (date._d.getDay() == 0 || date._d.getDay() == 1 || date._d.getDay() == 6 ) return true;
                        return false;
                    }
                });
            }   

            });
            
        
            
            @if(Session::has('openLabel') && ($Checkout[0]->country_id == 2) )
                window.open("https://dcm-info.nl/bestellen/carrierlabel/{{$Checkout[0]->invoice_id}}");
            @elseif(Session::has('openLabel') )
                window.open("https://dcm-info.be/nl/bestellen/carrierlabel/{{$Checkout[0]->invoice_id}}");
            @endif

            
            
        });

        function triggerbackorder(){
            $('.adminbackorder').show();
            return false;
        }
    </script>

@stop
