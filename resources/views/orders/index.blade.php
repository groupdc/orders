@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1> Orders </h1>

        <ol class="breadcrumb">
            <li class="active"><i class="far fa-shopping-basket"></i> Orders</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    <h2>Overview</h2>
                    <table id="datatable">
                        <thead>
                        <th>Reference</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Dealer</th>
                        <th>Vouchers</th>
                        <th>Payed</th>
                        <th>Country</th>
                        <th>Web vs API </th>
                        @can('orders-edit')
                            <th>Edit</th>
                        @endcan
                        </thead>
                    </table>

                <script type="text/javascript">
                    $(document).ready(function() {
                        oTable = $('#datatable').DataTable({
                            "pageLength": 50,
                            "processing": true,
                            "serverSide": true,
                            "ajax": "{{ route('admin/orders/api/table/orders') }}",
                            "columns": [{
                                    data: 'invoice_id',
                                    name: 'invoice_id',
                                    orderable: true,
                                    searchable: true
                                },
                                {
                                    data: 'name',
                                    name: 'name',
                                    orderable: true,
                                    searchable: true
                                },
                                {
                                    data: 'email',
                                    name: 'email',
                                    orderable: true,
                                    searchable: true
                                },
                                {
                                    data: 'dealer_code',
                                    name: 'dealer_code',
                                    orderable: true,
                                    searchable: true
                                },
                                {
                                    data: 'voucher',
                                    name: 'voucher',
                                    orderable: false,
                                    searchable: true
                                },
                                {
                                    data: 'payed',
                                    name: 'payed',
                                    orderable: false,
                                    searchable: true
                                },
                                {
                                    data: 'countryDB',
                                    name: 'countryDB',
                                    orderable: false,
                                    searchable: false
                                },
                                {
                                    data: 'origin',
                                    name: 'origin',
                                    orderable: false,
                                    searchable: true
                                },
                                @can('orders-edit') {
                                    data: 'edit',
                                    name: 'edit',
                                    orderable: false,
                                    searchable: false
                                },
                                @endcan
                            ]
                        });
                    });
                </script>

                    <link rel="stylesheet" type="text/css"
                          href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>


                </div>
            </div>
        </div>
    </div>

@stop
