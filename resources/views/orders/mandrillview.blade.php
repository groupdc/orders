
@if(isset($Mandrillresult) && count($Mandrillresult)>0)
    @foreach($Mandrillresult as $sendto)
        @if($sendto['email'] != 'bre@groupdc.be')
        <div class="sendto" style="@if($Checkout->email == $sendto['email']) color:green @endif">

            To: {{$sendto['email']}}
            <br/>
            From: {{$sendto['sender']}}
            <br/>
            Subject: {{$sendto['subject']}}
            <br/>
            State: {{$sendto['state']}} (sending status of this message: sent, bounced, rejected)
            <br/>
            Opens: {{$sendto['opens']}}
            <br/>
            <br/>
            <br/>
        </div>
        @endif
    @endforeach
@endif