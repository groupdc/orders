@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
      <h1> Sales </h1>


      <ol class="breadcrumb">
        <li class="active"><i class="far fa-shopping-basket"></i> Sales </li>
      </ol>
    </div>

    <div class="main-content">
    	<div class="row">
				<div class="col-md-12">
					<div class="main-content-block">

                <h2>Sales</h2>

                    Total order with pickup: {{$Checkout[0]->pickup_orders}}<br/>
                    - Total order with pickup BE: {{$Checkout[0]->pickup_orders_be}}<br/>
                    - Total order with pickup NL: {{$Checkout[0]->pickup_orders_nl}}<br/>
                    Total orders with delivery: {{$Checkout[0]->delivery_orders}}<br/>
                    - Total orders with delivery: {{$Checkout[0]->delivery_orders_be}}<br/>
                    - Total orders with delivery: {{$Checkout[0]->delivery_orders_nl}}<br/>
                    Total vouchers used: {{$Checkout[0]->vouchers}}<br/>
                    Total products without voucher: {{$Checkout[0]->products}}<br/>
                    Total orders using voucher *: {{$Checkout[0]->orders_using_voucher}}<br/>
                    Total orders NOT using voucher*: {{$Checkout[0]->orders_without_voucher}}<br/>
                    Total orders made: {{$Checkout[0]->orders}}
                    <br/>
                    <br/>* some order can overlap, when 1 product with voucher and another without voucher

                <h2>Orders / day</h2>
                  <table>
                  @foreach($HighVolumeDays as $HVD)
                      <tr><td>{{$HVD->cnt}}</td><td>{{$HVD->orderday}}</td></tr>
                  @endforeach
                </table>

                <h2>Top products (overall)</h2>
                <table>
                @foreach($TopProducts as $TP)
                    <tr><td>{{$TP->cnt}}</td><td>{{$TP->product_txt}}</td></tr>
                @endforeach
                </table>

                <h2>Top products (using voucher)</h2>
                <table>
                @foreach($TopProductsViaVoucher as $TP)
                    <tr><td>{{$TP->cnt}}</td><td>{{$TP->product_txt}}</td></tr>
                @endforeach
                </table>

                <h2>Top products (without voucher)</h2>
                <table>
                @foreach($TopProductsNoVoucher as $TP)
                    <tr><td>{{$TP->cnt}}</td><td>{{$TP->product_txt}}</td></tr>
                @endforeach
                </table>

                <h2>Top Dealers BE</h2>
                <table>
                @foreach($TopDealersBE as $TD)
                    <tr><td>{{$TD->cnt}} orders / {{$TD->cntproducts}} products</td>
                        <td>{{$TD->code}}</td>
                        <td>{{$TD->dealer}}</td>
                        <td>{{$TD->address}}</td>
                        <td>{{$TD->zip}}</td>
                        <td>{{$TD->city}}</td>
                    </tr>
                @endforeach
                </table>

              <h2>Top Dealers NL</h2>
              <table>
              @foreach($TopDealersNL as $TD)
                  <tr><td>{{$TD->cnt}} orders / {{$TD->cntproducts}} products</td>
                      <td>{{$TD->code_NL}}</td>
                      <td>{{$TD->dealer}}</td>
                      <td>{{$TD->address}}</td>
                      <td>{{$TD->zip}}</td>
                      <td>{{$TD->city}}</td>
                  </tr>
              @endforeach
              </table>
	      	</div>
      	</div>
      </div>
    </div>

@stop
