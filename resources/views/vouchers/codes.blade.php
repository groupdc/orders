@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1> Orders </h1>

        <ol class="breadcrumb">
            <li class="active"><i class="far fa-shopping-basket"></i> Orders</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    <h2>Overview</h2>
                    <table id="datatable">
                        <thead>
                        <th>Code</th>
                        <th>Product</th>
                        <th>Used</th>
                        @can('orders-edit')
                            <th>Edit</th>
                        @endcan
                        </thead>
                    </table>

                        <script type="text/javascript">
                        $(document).ready(function () {
                            oTable = $('#datatable').DataTable({
                                "pageLength": 50,
                                "processing": true,
                                "serverSide": true,
                                "ajax": "{{ route('admin/vouchers/api/table/vouchers') }}",
                                "columns": [
                                    {data: 'code', name: 'code', orderable: true, searchable: true},
                                    {data: 'product_txt', name: 'product_txt', orderable: true, searchable: true},
                                    {data: 'used', name: 'used', orderable: true, searchable: false},
                                    @can('orders-edit')
                                    {data: 'edit', name: 'edit', orderable: false, searchable: false},
                                    @endcan
                                ]
                            });
                        });
                    </script>

                    <link rel="stylesheet" type="text/css"
                          href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>


                </div>
            </div>
        </div>
    </div>

@stop
